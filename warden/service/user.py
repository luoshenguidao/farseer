from flask_jwt_extended import create_access_token
from sqlalchemy import func

from warden.model.common import DBSession
from warden.model.user import User
import hashlib

from warden.serializer.common import Serializer

session = DBSession()


class UserService:
    def __init__(self):
        pass

    @staticmethod
    def register(data):
        username = data.get("username", None)
        password = str(data.get("password", None))
        if username is None or password is None:
            return "用户名或密码不能为空"
        password_sha256 = hashlib.sha256(password.encode('utf8')).hexdigest()
        check_username = session.query(User).filter_by(username=username).first()
        if check_username is None:
            records = User(username=username, password=password_sha256, created_at=func.now())
            session.add(records)
            session.commit()
            session.close()
            return "添加成功"

        return "用户已存在，请修改用户名"

    @staticmethod
    def login(data):
        username = data.get("username", None)
        password = data.get("password", None)
        if username is None or password is None:
            code, msg = 40001, "用户名或密码不能为空"
            return Serializer.response(code, msg)
        password_sha256 = hashlib.sha256(password.encode('utf8')).hexdigest()
        check_records = session.query(User).filter_by(username=username).first()
        if check_records is not None:
            check_to_dict = check_records.as_dict_one()
            if check_to_dict.get("deleted_at", None):
                code, msg = 40002, "用户已销户"
                return Serializer.response(code, msg)
            if password_sha256 == check_to_dict.get("password", None):
                access_token = create_access_token(identity=username)
                code, msg, data = 20000, "登录成功", {'token': access_token}
                session.close()
                return Serializer.response(code, msg, data=data)
            code, msg = 40001, "用户或密码不正确"
            return Serializer.response(code, msg)
        code, msg = 40002, "用户不存在"
        return Serializer.response(code, msg)

    @staticmethod
    def show(username):
        check_records = session.query(User).filter_by(username=username).first()
        check_to_dict = check_records.as_dict_one()
        del check_to_dict['password']
        code, data = 20000, check_to_dict
        session.close()
        return Serializer.response(code, data=data)
