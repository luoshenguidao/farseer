import datetime

from sqlalchemy import func
from warden.model.common import DBSession
from warden.model.mysqlinfo import Mysql
from warden.serializer.common import Serializer

session = DBSession()


class MysqlService:
    def __init__(self, data):
        self.data = data

    def show(self):
        address = self.data.get("address", None)
        try:
            if address:
                check_records = session.query(Mysql).filter(None == Mysql.deleted_at).all()
                total = session.query(Mysql).filter(None == Mysql.deleted_at).count()
            else:
                check_records = session.query(Mysql).filter(None == Mysql.deleted_at).all()
                total = session.query(Mysql).filter(None == Mysql.deleted_at).count()
            check_to_dict = Serializer(check_records).as_json()
            result = {'total': total, 'items': check_to_dict}
            code, data = 20000, result
            return Serializer.response(code, data=data)
        except Exception as e:
            return Serializer.response(message=e)
        finally:
            session.close()

    def create(self):
        address = self.data.get("address", None)
        port = self.data.get("port", None)
        vip = self.data.get("vip", None)
        ip = self.data.get("ip", None)
        try:
            records = Mysql(address=address, port=port, vip=vip, ip=ip, created_at=func.now())
            session.add(records)
            session.commit()
        except Exception as e:
            return Serializer.response(message=e)
        finally:
            session.close()
        return Serializer.response(message="添加成功")

    def update(self):
        mysql_id = self.data.get("id", None)
        self.data.update({"updated_at": datetime.datetime.now()})
        result = session.query(Mysql).filter(Mysql.id == mysql_id).update(self.data)
        session.commit()
        session.close()
        return Serializer.response(message="修改成功", data=result)

    def delete(self):
        mysql_id = self.data.get("id", None)
        self.data.update({"deleted_at": datetime.datetime.now()})
        session.query(Mysql).filter(Mysql.id == mysql_id).update(self.data)
        session.commit()
        session.close()
        return Serializer.response(message="修改成功")
