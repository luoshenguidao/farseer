class CommonService:
    def __init__(self, vendors):
        self.vendors = vendors

    def as_json(self):
        return [item.as_dict_all() for item in self.vendors]
