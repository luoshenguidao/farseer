class Serializer:
    def __init__(self, vendors=None):
        self.vendors = vendors

    def as_json(self):
        return [item.as_dict_all() for item in self.vendors]

    @staticmethod
    def response(code=20000, message=None, data=None):
        result = {'code': code,
                  'message': message,
                  'data': data
                  }
        return result
