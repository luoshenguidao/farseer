from flask import jsonify, request
from flask import Blueprint
from warden.service.mysqlinfo import MysqlService

mysql = Blueprint('mysql', __name__)


@mysql.route('/show')
def show():
    result = MysqlService(request.args.to_dict()).show()
    return jsonify(result)


@mysql.route('/create', methods=['POST'])
def create():
    commit_json = request.json
    result = MysqlService(commit_json).create()
    return jsonify(result)


@mysql.route('/update', methods=['POST'])
def update():
    result = MysqlService(request.json).update()
    return jsonify(result)


@mysql.route('/delete', methods=['POST'])
def delete():
    result = MysqlService(request.json).delete()
    return jsonify(result)
