from flask import jsonify
from warden.model.dbconnection import operation
from flask import Blueprint

table = Blueprint('table', __name__)


@table.route('list')
def table_list():
    result = operation('select * from domain_info')

    total = operation('select count(*) as total from domain_info')
    total = total.get('total')
    data = {'code': 20000,
            'message': '',
            'data':
                {'total': total,
                 'items': result}
            }
    return jsonify(data)
