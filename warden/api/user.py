from flask import jsonify, request

from flask_jwt_extended import jwt_required, get_jwt_identity
from warden.serializer.common import Serializer
from flask import Blueprint
from warden.service.user import UserService

user = Blueprint('user', __name__)


@user.route('/login', methods=['POST'])
def login():
    commit_json = request.json
    result = UserService.login(commit_json)
    return jsonify(result)


@user.route('/info')
@jwt_required
def info():
    current = get_jwt_identity()
    result = UserService.show(current)
    return jsonify(result)


@user.route('/logout', methods=['POST'])
@jwt_required
def logout():
    msg = '用户登出成功'
    result = Serializer().response(message=msg)
    return jsonify(result)


@user.route('/register', methods=['POST'])
def register():
    commit_json = request.json
    msg = UserService.register(data=commit_json)
    return Serializer().response(message=msg)
