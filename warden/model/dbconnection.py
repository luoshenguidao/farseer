import records
from warden.conf.db_setting import ADDRESS


def operation(sentence, multi=True):
    db = records.Database(ADDRESS)
    row = db.query(sentence)
    if multi:
        result = row.as_dict()
    else:
        result = row.first().as_dict()
    return result
