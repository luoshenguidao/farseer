import datetime

from sqlalchemy import Column, Integer, String, TIMESTAMP, text
from .common import Base, engine


class Mysql(Base):
    __tablename__ = 'war_mysql'
    id = Column(Integer, autoincrement=True, primary_key=True)
    address = Column(String(128), unique=True)
    port = Column(String(6))
    vip = Column(String(128))
    ip = Column(String(128))
    created_at = Column(TIMESTAMP)
    updated_at = Column(TIMESTAMP,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    deleted_at = Column(TIMESTAMP)

    def __repr__(self):
        return "<Mysql(address='%s', port='%s', vip='%s',ip='%s'," \
               "created_at='%s',updated_at='%s',deleted_at='%s')>" \
               % (self.address, self.port, self.vip, self.ip,
                  self.created_at, self.updated_at, self.deleted_at)

    def as_dict_one(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def as_dict_all(self):
        result = {}
        for key in self.__mapper__.c.keys():
            value = getattr(self, key)
            result[key] = str(value) if value is not None else value
        return result


Base.metadata.create_all(engine)
