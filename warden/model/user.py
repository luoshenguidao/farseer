import datetime

from sqlalchemy import Column, Integer, String, TIMESTAMP
from .common import Base, engine


class User(Base):
    __tablename__ = 'war_ucenter'
    id = Column(Integer,autoincrement=True, primary_key=True)
    username = Column(String(255))
    password = Column(String(255))
    roles = Column(String(64))
    introduction = Column(String(255))
    avatar = Column(String(255))
    name = Column(String(255))
    created_at = Column(TIMESTAMP)
    updated_at = Column(TIMESTAMP)
    deleted_at = Column(TIMESTAMP)

    def __repr__(self):
        result = f"User(id={self.id},username={self.username},password={self.password})," \
              f"role={self.roles},introduction={self.introduction},avatar={self.avatar}," \
              f"name={self.name},created_at={self.created_at},updated_at={self.updated_at}," \
              f"deleted_at={self.deleted_at}"
        return result

    def as_dict_one(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def as_dict_all(self):
        result = {}
        for key in self.__mapper__.c.keys():
            value = getattr(self, key)
            result[key] = str(value) if value is not None else value
        return result


Base.metadata.create_all(engine)

