from sqlalchemy import create_engine
from warden.conf.db_setting import ADDRESS, ECHO
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

engine = create_engine(ADDRESS, echo=ECHO)
Base = declarative_base()

DBSession = sessionmaker(bind=engine)
