from flask import Flask
from flask_cors import CORS
from flask_jwt_extended import JWTManager

from warden.api.table import table
from warden.api.user import user
from warden.api.mysqlinfo import mysql


def create_app():
    app = Flask(__name__)
    CORS(app)
    app.config.from_object('warden.conf.secure')
    reg_blueprint(app)
    JWTManager(app)
    return app


def reg_blueprint(app):
    app.register_blueprint(user, url_prefix='/api/user')
    app.register_blueprint(table, url_prefix='/article')
    app.register_blueprint(mysql, url_prefix='/api/mysql')
